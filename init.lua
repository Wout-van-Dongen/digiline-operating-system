--[[ Main Operating System Driver code 
This consist of main code that will be run on a lua controller.
]]

function contains(list, x)
	for _, v in pairs(list) do
		if v == x then return true end
	end
	return false
end

function reset()
    --We still use 2 static arrays for saving files.
    --instead we could use one dynamic, where the indices are the filenames in the next version
    mem.files = {"", "", "", "", "", ""} --The files contain the raw text
    mem.names = {"", "", "", "", "", ""} --These are the filenames assigned to the raw texts
    mem.i = 0
    mem.storage = #mem.files --"Avialable memory". When you write a file, reduce by 1. Should be 0 when "files" is full
    mem.size = #mem.files --max storage size, this is a constant
    --enable command mode per default, disavle all other modes
    mem.command = true
    mem.write=false
    mem.dsend = false
    mem.dsendch = "d"
    digiline_send("lcd", "System reseted")
end

--============MAIN===============
if event.type == "program" then
    reset()

elseif event.type == "digiline" then
    if(event.channel == "input" )then  --print the typed command
        digiline_send("lcd",">"..event.msg) 
    end

    if (event.channel == "reset" and event.msg == "reset")then
        reset()
    end

--When you enter text, the porgramm needs to know how to deal with it. 
--Is it a command? Is it some raw text? A password? Or numbers for calculation?
--Most of the time, we just have commands with arguments, like >write [test.txt]
--But sometimes, when we are running a "texteditor", "calculator" or whatever
--The expected input is too long to be just a single-worded argument.
--We therefore introduce variables of the type 'logic' for the diferent modes.
--When we get input, if-statements check which mode is applied.
--When you are done, dont forget to restore the original settings:
--mem.command=true, mem.[currentmode]=false


--WRITE MODE
--text is just added to the existing one. This way we avoid overwriting files.
--and if its a new file, dont worry- the text is then added to an empty string.
    if(event.channel == "input" and mem.write and not mem.command)then
        mem.files[mem.j] = mem.files[mem.j]..event.msg
        mem.write = false
        mem.command = true
        digiline_send("lcd", "file "..mem.names[mem.j].." saved")
    --Digiline Send mode
    elseif (event.channel == "input" and mem.dsend and not mem.command) then
        digiline_send(mem.dsendchannel,event.msg)
        mem.dsend= false
        mem.command = true

    --COMMAND MODE
    --List files
    elseif ((event.msg == "ls" or event.msg == "ls -l" or event.msg == "list")and mem.command) then
        mem.j = 1
        while(mem.j <= 6)do
            digiline_send("lcd", mem.names[mem.j])
            mem.j = mem.j + 1
        end

    
  --read files
    elseif(event.channel == "input" and string.sub(event.msg, 1, 4) == "read" and mem.command)then
        --we check if the first characters are "write". The rest (minus the space) is our filename=target
        --then scroll trough our files and display if the name matches
        mem.target=string.sub(event.msg,6)
        mem.j = 1
        while(mem.j <= mem.size)do
        if (mem.names[mem.j] == mem.target) then break end
            mem.j = mem.j+1
        end
        digiline_send("lcd", mem.files[mem.j])

    
  --Write files
    elseif(event.channel=="input" and string.sub(event.msg,1,5)=="write" and mem.command)then
        --Check if file already exists.
        mem.target=string.sub(event.msg,7) --file name
        mem.j=1
        mem.found=false
        while(mem.j<=mem.size)do
        if (mem.names[mem.j]==mem.target) then
            mem.found=true -- therefore the file exist
            digiline_send("lcd","Enter your text")
            mem.write=true  -- write mode is set to true
            mem.command=false -- command mode is set to false 
            break
        end
        mem.j=mem.j+1
        end
        --- if file not found =>make a new one...if we still have memory available:
        if ((not mem.found) and mem.storage<1) then  
        digiline_send("lcd", "No space")  --storage limit  is 6 files
        end
        if ((not mem.found) and mem.storage>=1) then 
        mem.j = 1
        --use a loop to find the next empty file. we start with j=1 and check from the bottom to the top
        --if any slot of the list is available. That way, we can recycle empty slots in between like ["bla","lol","","hi","",""]
        --after an deletetion of its content 
        while(mem.j <= mem.size)do
            if mem.files[mem.j] == "" then 
            break 
            end
            mem.j= mem.j+1
        end
        mem.names[mem.j] = mem.target
        digiline_send("lcd", "Enter your text")
        --toggle write mode
        mem.write = true
        mem.command = false
        mem.storage= mem.storage - 1     --we wrote a new file- and occupied an entry of the file list. reduce storage 
        end
        if (mem.found)then 
        digiline_send("lcd", "Continue on\n"..mem.target) 
        end

  --Delete files
    elseif (event.channel=="input" and string.sub(event.msg,1,6) == "delete" and mem.command) then
        mem.target=string.sub(event.msg,8)
        mem.j=1
        while mem.j<=mem.size do
        if mem.names[mem.j] == mem.target then
            mem.names[mem.j] = ""
            mem.files[mem.j] = ""
            digiline_send("lcd", "file deleted!")
            mem.storage = mem.storage + 1
        end
        mem.j = mem.j + 1
        end
    --help
    elseif(event.channel == "input" and event.msg == "help" and mem.command) then
        digiline_send("lcd","Commands:\nlist, screensaver\nread\nwrite\ndelete\nturnon\nturnoff\ntime\ndate\nsetch\ndsend")
    --Screensave
    elseif(event.channel == "input" and event.msg == "screensaver" and mem.command) then
        digiline_send("lcd", "______ _\n| ___ \\ | Operating\n| |_/ / |_ System__\n| ___ \\ | | ||/ _ \\\n| |_/ / | |_||  __/\n\\____/|_|\\__,|\\___|")  

    --Mesecon-Interace
    elseif(event.channel == "input" and string.sub(event.msg,1,6) == "turnon" and mem.command ) then 
      -- Turn on ports
        if (string.sub(event.msg, 8) == 'a') then
            port.a = true
        elseif (string.sub(event.msg, 8) == 'b') then
            port.b = true
        elseif (string.sub(event.msg, 8) == 'c') then
            port.c = true
        elseif (string.sub(event.msg, 8) == 'd') then
            port.d = true   
        else
            digiline_send("lcd" ,"Invalid port")
        end
    
    elseif(event.channel == "input" and string.sub(event.msg, 1, 7) == "turnoff" and mem.command ) then
      -- Turn off ports
        if (string.sub(event.msg, 9) =='a') then
        port.a = false
        elseif (string.sub(event.msg, 9) == 'b') then
            port.b = flase
        elseif (string.sub(event.msg, 9) == 'c') then
            port.c = false
        elseif (string.sub(event.msg, 9) == 'd') then
            port.d = false 
        else
            digiline_send("lcd" ,"Invalid port")
        end
    
    elseif( event.channel == "input" and string.sub(event.msg, 1, 4) == "time" and mem.command) then
        -- Show time
        time = os.datetable()
        digiline_send("lcd",time.hour..":"..time.min..":"..time.sec.." UTC")
    
    elseif( event.channel == "input" and string.sub(event.msg, 1, 4) == "date" and mem.command) then
        -- show date
        date = os.datetable()
        digiline_send("lcd",date.day.."/"..date.month.."/"..date.year)


    --Communication Protocol    
    elseif ( event.channel == "input" and string.sub(event.msg, 1, 5) == "dsend" and mem.command) then
        mem.dsend = true
        mem.command = false
        mem.dsendchannel = string.sub(event.msg, 7)
        digiline_send("lcd","Enter the message to be sent>")
    elseif(event.channel == "input" and string.sub(event.msg,1,5)=="setch" and mem.command) then
        mem.dsendch = string.sub(event.msg,7)
        digiline_send("lcd", "channel set to "..mem.dsendch)
    elseif (event.channel == mem.dsendch ) then
        digiline_send("lcd","message from connected computer: "..event.msg)
        
    else
     --   digiline_send("lcd" ,"Unknown Command")
    end
end
